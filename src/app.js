import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  FlatList,
  View
} from 'react-native';

import Post from './components/Post';

/*
const fotos = [
  {
    id: 1,
    usuario: 'rrgomide'
  },
  {
    id: 2,
    usuario: 'marcelle'
  },
  {
    id: 3,
    usuario: 'leila'
  },
];
*/

class ReactNativeAluraParte01 extends Component {

  constructor() {
    super();
    this.state = {
      fotos: []
    };
  }

  componentDidMount() {
    fetch('https://instalura-api.herokuapp.com/api/public/fotos/rafael')
      .then(resposta => resposta.json())
      .then(json => this.setState({ fotos: json }));
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList        
          data={this.state.fotos}
          renderItem={({ item }) => <Post foto={item} />}
          keyExtractor={item => item.id}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    margin: 10
  },
});

export default() => {
  AppRegistry
    .registerComponent(
      'ReactNativeAluraParte01', 
      () => ReactNativeAluraParte01
    );
};
