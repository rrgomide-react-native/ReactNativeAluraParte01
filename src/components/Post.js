import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  TextInput,
  TouchableOpacity,
} from 'react-native';

const imgCoracaoVazio = require('../../resources/img/s2.png');
const imgCoracaoCheio = require('../../resources/img/s2-check.png');
const imgEnviar = require('../../resources/img/send.png');

const largura = Dimensions.get('screen').width;

export default class Post extends Component {

  constructor(props) {
    super(props);

    this.state = {
      //foto: {...this.props.foto, likers: [{}]}
      foto: this.props.foto,
      valorComentario: ''
    }
  }

  adicionaComentario() {

    if (this.state.valorComentario === '')
      return;

    const novaLista = [
      ...this.state.foto.comentarios, 
      {
        id: this.state.foto.comentarios.length + 1,
        login: 'meuUsuario',
        texto: this.state.valorComentario,
      }
    ];

    const fotoAtualizada = {
      ...this.state.foto,
      comentarios: novaLista,
    }    

    this.setState({foto: fotoAtualizada, valorComentario: ''});
    this.inputComentario.clear();
  }

  curtir() {

    const { foto } = this.state;

    let novaLista = [];

    if(!foto.likeada) {
      novaLista = [
        ...foto.likers,
        {login: 'meuUsuario'}
      ];
    }
    else {
      novaLista = foto.likers.filter(liker => {
        return liker.login !== 'meuUsuario';
      });
    }

    const fotoAtualizada = {
      ...this.state.foto,
      likeada: !this.state.foto.likeada,
      likers: novaLista
    }

    this.setState({foto: fotoAtualizada});
  }

  carregaIcone(likeada) {
    return (likeada) 
      ? imgCoracaoCheio 
      : imgCoracaoVazio;
  }

  exibeLikes(likers) {

    if (likers.length <= 0)
      return;

    return (
      <Text style={styles.likeadas}>
        {likers.length} curtida(s)
      </Text>
    );
  }

  exibeLegenda(foto) {

    if (foto.comentario === '')
      return;
      
    return (
      <View style={styles.comentario}>
        <Text style={styles.tituloComentario}>
          {foto.loginUsuario}
        </Text>
        <Text>
          {foto.comentario}
        </Text>          
      </View> 
    );   
  }

  render() {

    const { foto } = this.state;

    return (
      <View>
        <View style={styles.cabecalho}>
          <Image 
            source={{ uri: foto.urlPerfil }} 
            style={styles.thumbnail}        
          />          
          <Text>{foto.loginUsuario}</Text>
        </View>
        <Image 
          source={{ uri: this.props.foto.urlFoto }}
          style={styles.imagem}        
        />  
        <View style={styles.rodape}>
          <TouchableOpacity onPress={this.curtir.bind(this)}>
            <Image 
              style={styles.botaoLike}
              source={this.carregaIcone(foto.likeada)} />
          </TouchableOpacity>

          {this.exibeLikes(foto.likers)}
          {this.exibeLegenda(foto)}          

          
          {            
            foto.comentarios.map(comentario =>
              <View style={styles.comentario} key={comentario.id}>
                <Text style={styles.tituloComentario}>{comentario.login}</Text>
                <Text>{comentario.texto}</Text>
              </View>
            )            
          }

          <View style={styles.viewComentarios}>
            <TextInput 
              style={styles.input}
              placeholder='Comente aqui...'
              ref={input => this.inputComentario = input}
              onChangeText={texto => this.setState({valorComentario: texto})}
            /> 
            <TouchableOpacity
              onPress={this.adicionaComentario.bind(this)}
            >
              <Image
                style={styles.iconeEnvio}
                source={imgEnviar}
              />                   
            </TouchableOpacity>
          </View>

        </View> 
      </View>
    );
  }
}

const styles = StyleSheet.create({

  cabecalho: {     
    flexDirection: 'row', 
    alignItems: 'center',
    marginBottom: 10 
  },

  thumbnail: {
    marginRight: 10, 
    borderRadius: 20, 
    width: 40, 
    height: 40
  },

  imagem: {
    width: largura, 
    height: largura    
  },

  botaoLike: {
    marginBottom: 10,
    height: 40,
    width: 40
  },

  rodape: {
    margin: 10
  },

  likeadas: {
    fontWeight: 'bold'
  },

  comentario: {
    flexDirection: 'row'
  },

  tituloComentario: {
    fontWeight: 'bold',
    marginRight: 5
  },

  input: {
    flex: 1,
    height: 40
  },

  viewComentarios: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: '#ddd'
  },

  iconeEnvio: {
    width: 30, 
    height: 30
  }
});

AppRegistry
  .registerComponent(
    'Post', 
    () => Post
  );
